<h1 align="center">BlockScout</h1>
<p align="center">Blockchain Explorer for inspecting and analyzing EVM Chains.</p>
## Installation 

https://computingforgeeks.com/how-to-install-latest-erlang-on-ubuntu-linux/

```
sudo apt update
sudo apt install curl software-properties-common apt-transport-https lsb-release
curl -fsSL https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/erlang.gpg
echo "deb https://packages.erlang-solutions.com/ubuntu $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/erlang.list
sudo apt install erlang elixir

wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

sudo apt install make 
sudo apt install g++

```
```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

```
nvm install 16
```

`apt-get install inotify-tools`
`\curl -sSL https://raw.githubusercontent.com/taylor/kiex/master/install | bash -s` 
`apt-get install cargo`

Use the blockscout now

```
https://docs.blockscout.com/for-developers/manual-deployment
```

`mix do deps.get, local.rebar --force, deps.compile`

https://docs.blockscout.com/for-developers/information-and-settings/requirements
https://wiki.polygon.technology/docs/edge/additional-features/blockscout/

## Deployment Steps
1. git clone https://gitlab.com/john-accubits/kyoto-explorer.git
2. cd blockscout
3. Set other environment variables as needed.
```
export DATABASE_URL=postgresql://postgres:KyotoDatabase123@kyoto-testnet-in.cncgwnn8ns9y.ap-south-1.rds.amazonaws.com:5432/blockscout
export ETHEREUM_JSONRPC_VARIANT=besu
export COIN=POA
export COIN_NAME=KYO
export ETHEREUM_JSONRPC_HTTP_URL=http://3.6.235.72:8545
export ETHEREUM_JSONRPC_TRACE_URL=http://3.6.235.72:8545
export SECRET_KEY_BASE=VTIB3uHDNbvrY0+60ZWgUoUBKDn9ppLR8MI4CpRz4/qLyEFs54ktJfaNT6Z221No
export DISABLE_EXCHANGE_RATES=true
export DISABLE_READ_API=true
export DISABLE_WRITE_API=true
export SUBNETWORK=Kyoto
export SUPPORTED_CHAINS=[]
```
4. Install Mix dependencies and compile them `mix do deps.get, local.rebar --force, deps.compile`
5. If you have deployed previously, remove static assets from the previous build    
`mix phx.digest.clean`.
6. Compile the application:
`mix compile`
7. Create and migrate database `mix do ecto.create, ecto.migrate`
note: If you are in dev environment and have run the application previously with a different blockchain, drop the previous database `mix do ecto.drop, ecto.create, ecto.migrate`
Be careful since it will delete all data from the DB. Don't execute it on production if you don't want to lose all the data!
8. Install Node.js dependencies
- `cd apps/block_scout_web/assets; npm install && node_modules/webpack/bin/webpack.js --mode production; cd -`
- `cd apps/explorer && npm install; cd -`
9. Build static assets for deployment mix phx.digest
10. Enable HTTPS in development. The Phoenix server only runs with HTTPS.
`cd apps/block_scout_web; mix phx.gen.cert blockscout blockscout.local; cd -`
Add blockscout and blockscout.local to your /etc/hosts
```
   127.0.0.1       localhost blockscout blockscout.local

   255.255.255.255 broadcasthost

   ::1             localhost blockscout blockscout.local
```
If using Chrome, Enable chrome://flags/#allow-insecure-localhost
11. Return to the root directory and start the Phoenix Server. 
```
mix phx.server
```
12. To run in detached mode so that the Phoenix server does not stop and continues to run even if you close the terminal:
```
elixir --erl "-detached" -S mix phx.server     
```